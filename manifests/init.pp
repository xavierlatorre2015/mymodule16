class mymodule16 {
    include '::mymodule16::pm_apache'
    include '::mymodule16::pm_php'
    include '::mymodule16::pm_mysql'
    include '::mymodule16::pm_time'
    include '::mymodule16::pm_mongodb'
    include '::mymodule16::pm_files'
    include '::mymodule16::pm_firewall'
}
